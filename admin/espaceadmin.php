<?php 
session_start (); ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="../main.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Luxurious+Roman&display=swap" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet"> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<?php include('headerAdmin.html');?>
<body>
        

<div class="mesliens">
<?php


    // On démarre la session (ceci est indispensable dans toutes les pages de notre section membre)


    // On récupère nos variables de session
    if (isset($_SESSION['username']) && isset($_SESSION['psw'])) {
?>
    
        <a href="commandes.php" >Commandes</a>
        <a href="information.php" >Informations </a>
        <a href="produits.php" > Ajouter/modifier produit </a>
        <a href="suppression.php" >Supprimer client </a>
    

    	<a href="./logout.php">Déconnexion</a>
   <?php }
    else {
    	echo 'Les variables ne sont pas déclarées.';
    }
    ?>
</div>
</body>
</html>