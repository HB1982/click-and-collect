<?php
// On démarre la session (ceci est indispensable dans toutes les pages de notre section membre)
session_start();

// On récupère nos variables de session
if (isset($_SESSION['username']) && isset($_SESSION['psw'])) { ?>



    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link href="../main.css" rel="stylesheet">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Luxurious+Roman&display=swap" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>

    <body onload="majTotal(input_qt)">
   
        <?php include 'headerAdmin.html'; ?>
        <?php
        include('../database.php');
        $idCommande = $_GET['id'];
        $resultat = getCommande($idCommande);
        $idClient = auteurcommande($idCommande);
        $coordonnees = recupererCoordonnees($idClient);

        ?>

        <div >
            <h2>Commande : <?php echo $idCommande; ?> </h2>
            <div class="Infos">
                <h3>Informations générales</h3>
                <ul>
                    <li>
                        Nom : <?php echo $coordonnees['nom'];
                                ?>

                    </li>
                    <li>
                        Email : <?php echo $coordonnees['email']; ?>
                    </li>
                    <li>
                        Téléphone : <?php echo $coordonnees['telephone']; ?>
                    </li>
                </ul>
            </div>
           <div class="boxproduit">
            <?php foreach ($resultat as $i) {
            ?>
                <div class="column">
                    <?php $Produit = getProduit($i['idProduit']);
                    ?>
                    <p>Produit : <?php echo $Produit['nom']; ?></p>
                    <img class="imageNormal" src="<?php echo $Produit['image']; ?>" alt="<?php echo $Produit['nom'] . ".jpg"; ?>">
                    <p>Quantité : <?php echo $i['quantite']; ?></p>


                    <input id="<?php echo $Produit['id']; ?>" class="produitqt" data-prix="<?php echo $Produit['prixAuKg']; ?>" name="<?php echo $Produit['id']; ?>"  value="<?php echo $i['quantite']; ?> "hidden>
                </div>
                <?php
            }
          
                ?> 

                </div>
                <div>
                    <form action="modifEtat.php" method="post">
                        <label for="etat">Etat de la commande:</label>
                        <select type="select" name="etat">

                            <option value="validee">Validée</option>
                            <option value="prete">Prête</option>
                            <option value="collectee">Collectée</option>
                        </select>
                        <input type="text" value="<?php echo $idCommande; ?>" name="idCommande" hidden>
                        <input type="text" value="<?php echo ($coordonnees['email']); ?>" name="email" hidden>
                        <p>Total: <span id="total">0</span>Euros</p>
                        <button type="submit">Valider</button>

                    </form>

                </div>

        </div>
    </body>
    <script type="text/javascript" src="../Panier.js"></script>
    </html>


<?php  } ?>