<?php
// On démarre la session (ceci est indispensable dans toutes les pages de notre section membre)
session_start();

// On récupère nos variables de session
if (isset($_SESSION['username']) && isset($_SESSION['psw'])) { ?>




    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link href="../main.css" rel="stylesheet">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Luxurious+Roman&display=swap" rel="stylesheet">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <?php include('headerAdmin.html');
    include('../database.php'); ?>

    <body>
        <?php
        $resultat =  afficherCommandes();
        ?>
        
            <h2>Liste des commandes</h2>
            <div class="boxproduit">
            <?php
            foreach ($resultat as $i) {
            ?>
                <form action="commande.php?id=<?php echo ($i['id']); ?>" method="post">
                    <div>
                        <button class="btncommande ">
                            <p>commande n°:<?php echo $i['id']; ?></p>
                            <p>statut:<?php echo $i['EtatCommande']; ?></p>
                        </button>
                    </div>
                </form>
            <?php } ?>
        </div>

    </body>

    </html>

<?php  } ?>