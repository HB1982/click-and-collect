<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="main.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Luxurious+Roman&display=swap" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulaire d'identification</title>
</head>
<?php include('header.html'); ?>

<body>
    <form class="column" id="centrer" action="admin/login.php" method="post">
        <p>Votre login :</p> <input type="text" name="username" required>
        <p>Votre mot de passe :</p> <input type="password" name="psw" required>
        <input class="btncommande" type="submit" value="Connexion">
    </form>

</body>

</html>