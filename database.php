<?php



$host = "localhost";
$db   = 'clickAndCollect';
$user = "Bob";
$pass = "toto123";


$pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);



function getInfos()
{
    global $pdo;


    $req = $pdo->prepare('Select * from Information;');
    $req->execute();
    return $req->fetch();
}



function getProduits()
{
    global $pdo;
    $req = $pdo->query('select * from Produit;');
    return $req->fetchAll();
}

function produitsdispos()
{
    global $pdo;
    $req = $pdo->query('select * from Produit where disponibilité=1;');
    return $req->fetchAll();
}


function getProduit($idProduit)
{
    global $pdo;


    $req = $pdo->prepare('Select * from Produit where id = ? ;');
    $req->execute([$idProduit]);
    return $req->fetch();
}



function getCommande($idCommande)
{
    global $pdo;
    $req = $pdo->prepare('Select * from LigneCommande where idCommande = ? ;');
    $req->execute([(int)$idCommande]);
    //var_dump($req->debugDumpParams());
    return $req->fetchAll();
}

//  function ModifProduit -> addProduit (newproduit) + modifDispo(ancienproduit)

// Select * from produit where nom = banane 


function addProduit($nom, $image, $prixAuKg)
{
    // ajouter le produit $nom avec $qt
    // à la liste $produits
    global $pdo;
    $req = $pdo->prepare("insert into Produit (nom, image, prixAuKg, disponibilité) values (?, ?, ?, 1);");
    $req->execute([$nom, $image, $prixAuKg]);
}

function auteurcommande($idCommande)
{
    global $pdo;
    $req = $pdo->prepare('select * from Commande where id=?;');
    $req->execute([$idCommande]);
    $resultat = $req->fetch();
    return $resultat['idClient'];
}

function modifDispo($nom)
{
    global $pdo;
    $req = $pdo->prepare("Update Produit SET disponibilité = 0 where nom = ?;");
    $req->execute([$nom]);
}




function creerPanier()
{

    global $pdo;
    $req = $pdo->prepare("insert into Commande (EtatCommande) values ('panier');");
    $req->execute();
    return $pdo->lastInsertId();
}



function creerLigneCommande($idProduit, $idCommande, $quantite)
{

    global $pdo;
    $req = $pdo->prepare("insert into LigneCommande (idProduit, idCommande, quantite) values (?, ?, ?);");
    $req->execute([$idProduit, $idCommande, $quantite]);
}




function validerCommande($idCommande)
{

    global $pdo;
    $req = $pdo->prepare("UPDATE Commande SET EtatCommande = 'validee' WHERE id = ?;");
    $req->execute([$idCommande]);
}

function afficherCommandes()
{
    global $pdo;
    $req = $pdo->prepare("SELECT * FROM Commande  WHERE EtatCommande = 'validee' OR  EtatCommande='prete' ORDER BY id DESC;");
    $req->execute();
    return $req->fetchAll();
}
function preparerCommande($idCommande)
{
    global $pdo;
    $req = $pdo->prepare("UPDATE Commande SET EtatCommande = 'prete' WHERE id = ?;");
    $req->execute([$idCommande]);
}

// Dans le php, afficher toute les commandes prete ou validee
function collecterCommande($idCommande)
{

    global $pdo;
    $req = $pdo->prepare("UPDATE Commande SET EtatCommande = 'collectee' WHERE id = ?;");
    $req->execute([$idCommande]);
}

function ClientCommande($idClient, $idCommande)
{
    global $pdo;
    $req = $pdo->prepare("UPDATE Commande SET idClient = ? WHERE id = ?;");
    $req->execute([$idClient, $idCommande]);
}



function ajouterCoordonnes($nom, $email, $telephone)
{

    global $pdo;
    $req = $pdo->prepare("insert into CoordonnesClient (nom, email, telephone) values (?, ?, ?);");
    $req->execute([$nom, $email, $telephone]);
    return $pdo->lastInsertId();
}

function recupererCoordonnees($idClient)
{

    global $pdo;
    $req = $pdo->prepare("SELECT * FROM CoordonnesClient where id=?;");
    $req->execute([$idClient]);
    return $req->fetch();
}


function checkAdmin($nom)
{

    global $pdo;
    $req = $pdo->prepare("SELECT * FROM Administrateur where nom=?;");
    $req->execute([$nom]);
    return $req->fetch();
}


function modifInformations($adresse, $telephone, $horaire)
{

    global $pdo;
    $req = $pdo->prepare("UPDATE Information SET adresse = ?, telephone = ?, horaire = ?;");
    $req->execute([$adresse, $telephone, $horaire]);
}

function anonymisation($email)
{

    global $pdo;
   
    $req = $pdo->prepare('UPDATE CoordonnesClient SET nom = "anonyme", email=  "anonyme", telephone = "anonyme" WHERE email=? ;');
    $req->execute([$email]);
}



// Si demande rgpd, si plusieurs personnes ont le meme nom, test avec autre infos mais a voir plus tard etc

function supprimerCoordonnes($nom)
{

    global $pdo;
    $req = $pdo->prepare("delete from CoordonnesClient where nom=?");
    $req->execute([$nom]);
}


function ModifData($categorie, $value, $id)
{
    global $conn;
    $sql = "UPDATE Projet SET " . $categorie . " = ? WHERE id = ?;";
    echo $sql;
    $stmt = $conn->prepare($sql);

    $stmt->bind_param('si', $value, $id);
    $stmt->execute();
    $result = $conn->query($stmt);
}
  



//infos
