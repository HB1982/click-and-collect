let input_qt = document.getElementsByClassName("produitqt")
for (const input of input_qt) {
    input.addEventListener(
        'change',
        (event) => {
            majTotal(input_qt)

        }
    )
}

let total = document.getElementById('total')

function majTotal(produit) {
    let somme = 0
    for (const input of produit) {
        let prix = input.getAttribute('data-prix')
        let val = input.value
        somme += prix * val
    }
    total.innerText = somme
}