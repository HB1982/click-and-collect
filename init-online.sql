-- Importer ce fichier dans phpmyadmin à l'intérieur de la BDD que vous utiliser pour votre Click And Collect

CREATE TABLE Administrateur (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nom varchar(255),
    mdp varchar(100)
);

CREATE TABLE Produit(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nom varchar(255),
    image varchar(255),
    prixAuKg FLOAT,
    disponibilité TINYINT(1)
);

CREATE TABLE LigneCommande(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    idProduit INT(255),
    idCommande INT(255),
    quantite FLOAT
);

CREATE TABLE Commande(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    idClient INT (200),
    EtatCommande ENUM ('panier', 'validee', 'prete', 'collectee')
);



CREATE TABLE CoordonnesClient(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nom varchar(255),
    email varchar(255),
    telephone varchar(255)
);


CREATE TABLE Information(
    adresse varchar(255),
    telephone varchar(255),
    horaire varchar(255)
);



INSERT INTO Information (adresse, telephone, horaire) VALUES ("Avenue des champs-elysées", "0606060606", "8h30-12h30 puis 13h30-15h");
INSERT INTO Administrateur (nom, mdp) VALUES ("nomadmin", "mdpadmin");
-- Remplacer nomadmin et mdpadmin par les identifiants que vous souhaitez utiliser sur le site
