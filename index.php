<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Luxurious+Roman&display=swap" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet"> 
    <link href="main.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index</title>
</head>
<?php include('header.html'); ?>
<body>

    <?php
    
    include('database.php');
    $resultat = getInfos();


    ?>
    <div class="index">

        <div class="coordonnées">
            <img src="./images/3347e8759c9246808e6e73564f44c5a0.png" alt="monlogo">
            <h1>Direct Potager</h1>

            <div class="ath">
                <p> <?php echo ($resultat['adresse']); ?></p>



                <p> <?php echo ($resultat['telephone']); ?></p>

                <p> <?php echo ($resultat['horaire']); ?></p>
            </div>


            <p id="texte">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta labore dolores placeat unde vero libero mollitia, voluptates magnam debitis, ab minus. Consequatur officiis ab fugit similique quisquam distinctio officia veritatis?</p>
         <a href="admin.php">espace admin</a> </div> 
      
        <div class="presentation">


            <img src="https://www.mon-panier-bio.com/wp-content/uploads/2017/06/Panier-12-18.jpg" alt="legumes" style="width:700px;height:600px">
        </div>

    </div>
   

</body>
<?php include('admin/footer.php'); ?>
</html>