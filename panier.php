<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="main.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Luxurious+Roman&display=swap" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Panier</title>
</head>
<?php include('header.html'); ?>
<body onload="majTotal(input_qt)">
    <?php
    include('database.php');
    $idCommande = $_GET["id"];
    $resultat = getCommande($idCommande);

    ?>
    <div class="monpanier">
        <div class="formvalid">
            <img src="./images/3347e8759c9246808e6e73564f44c5a0.png" alt="monlogo">
            <p>Afin de valider votre panier,</p>
            <p> merci de compléter les informations ci-dessous</p>
            <form action="validationcommande.php" method="post">

                <div>
                    <label for="Nom">Nom:</label>
                    <input type="text" name="nom" required>
                </div>
                <div>
                    <label for="email">Email:</label>
                    <input type="email" name="email" required>
                </div>
                <div>
                    <label for="telephone">Telephone:</label>
                    <input type="text" name="telephone" required>
                </div>
                <input name="idcommande" value=<?php echo "$idCommande" ?> type="text" hidden>
                

        </div>




        <div class="Panier">
            <h3>Panier</h3>

            <p> commande n° : <?php echo $idCommande; ?></p>

            <div class="colonne commande">
                <?php

                foreach ($resultat as $i) {
                ?>

                    <?php $Produit = getProduit($i['idProduit']);
                    ?>
                    <p> <?php echo $Produit['nom']; ?> :

                    <?php echo $i['quantite']; ?> kg</p>
                    <input id="<?php echo $Produit['id']; ?>" class="produitqt" data-prix="<?php echo $Produit['prixAuKg']; ?>" name="<?php echo $Produit['id']; ?>" hidden value="<?php echo $i['quantite']; ?>">

                <?php
                }
                ?>
                <button class="btncommande">Valider la commande</button>

                <p>Total: <span id="total">0</span>Euros</p>
            </div>


            </form>
        </div>
    </div>
    <script type="text/javascript" src="Panier.js"></script>
</body>
<?php include('admin/footer.php'); ?>
</html>